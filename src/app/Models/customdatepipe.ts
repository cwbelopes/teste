import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'customDatePipe',
})
export class CustomDatePipe implements PipeTransform {
    transform(value: string) {
       var datePipe = new DatePipe("pt-br");
        return datePipe.transform(value, 'dd-MMMM-yyyy');
    }
}