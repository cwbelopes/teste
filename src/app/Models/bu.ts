export interface BU{
    id: number;
    name: string;
}

export const BUS: BU[] = [
    { id: 1, name: 'MCS' },
    { id: 3, name: 'BPS' },
    { id: 4, name: 'METRONIC' },
    { id: 5, name: 'ASSEMBLY' },
    { id: 6, name: 'CARD BODY' },
];