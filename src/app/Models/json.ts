import { Order } from "./order";

export interface Json 
{
    OK: boolean,
    MESSAGE: string,
    DATA: Order[],
    CONNECTED: boolean   
}