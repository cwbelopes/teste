import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MonitoramentoProducaoComponent } from './monitoramento-producao.component';

describe('MonitoramentoProducaoComponent', () => {
  let component: MonitoramentoProducaoComponent;
  let fixture: ComponentFixture<MonitoramentoProducaoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MonitoramentoProducaoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MonitoramentoProducaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
