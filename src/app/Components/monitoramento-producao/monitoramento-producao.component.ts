import { Component, OnInit } from '@angular/core';
import { Order } from '../../Models/order';
import { OrderService } from '../../Services/order.service';
import { ActivatedRoute, Router, NavigationEnd, Event as NavigationEvent } from '@angular/router';
import { BU, BUS } from '../../Models/bu';
import { filter } from 'rxjs';

@Component({
  selector: 'app-monitoramento-producao',
  templateUrl: './monitoramento-producao.component.html',
  styleUrls: ['./monitoramento-producao.component.css']
})
export class MonitoramentoProducaoComponent implements OnInit {
  bus = BUS;
  selectedBU?: BU;
  orders: Order[] = [];
  displayedColumns: string[] = ['isPriority', 'SalesOrder', 'WorkOrder','ProductionDate'];

  constructor(private orderService: OrderService,
    private route: ActivatedRoute,
    private router: Router) 
    {
      this.router.events
      .pipe(filter( event =>event instanceof NavigationEnd))
      .subscribe((event: NavigationEvent) => {
        const bu_id = Number(this.route.snapshot.paramMap.get('id'));
        this.getOrders(bu_id);
        this.selectedBU = this.bus.find(bu => bu.id === bu_id);
      });
    }

  ngOnInit(): void {    
  }

  getOrders(bu_id: number): void {
    this.orderService.getOrders(bu_id)
      .subscribe(response => this.orders = response);
  }
}

