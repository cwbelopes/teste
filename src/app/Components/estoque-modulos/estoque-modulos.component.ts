import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-estoque-modulos',
  templateUrl: './estoque-modulos.component.html',
  styleUrls: ['./estoque-modulos.component.css']
})
export class EstoqueModulosComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit(): void {
  }
}
