import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EstoqueModulosComponent } from './estoque-modulos.component';

describe('EstoqueModulosComponent', () => {
  let component: EstoqueModulosComponent;
  let fixture: ComponentFixture<EstoqueModulosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstoqueModulosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstoqueModulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
