import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs';
import { Json } from '../Models/json';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(private http: HttpClient) {  }

  getOrders(id: number) {
    const ordersUrl = `http://localhost/webapi/Api/CustomErpOrder/GetCreatedWorkorderByBusinessUnit?buId=${id}`;
    return this.http.get<Json>(ordersUrl)
    .pipe(map(res => res.DATA));
  }
}
