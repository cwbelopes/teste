import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { DatasComponent } from './Components/datas/datas.component';
import { EstoqueModulosComponent } from './Components/estoque-modulos/estoque-modulos.component';
import { MonitoramentoProducaoComponent } from './Components/monitoramento-producao/monitoramento-producao.component';
import { PrioridadesComponent } from './Components/prioridades/prioridades.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'monitoramento-producao', redirectTo: '/monitoramento-producao/3', pathMatch: 'full' },
  { path: 'monitoramento-producao/:id', component: MonitoramentoProducaoComponent},
  { path: 'datas', component: DatasComponent},
  { path: 'prioridades', component: PrioridadesComponent},
  { path: 'estoque-modulos', component: EstoqueModulosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
